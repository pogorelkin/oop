package main.Stud;

public class MarksCalculationService extends Group implements MarkCalculatonServiceInterface {

    @Override
    public float get_groupAverage_grade(){
        float result = 0;
        for (int i = 0; i < 12; i++) {
            result += students[i].get_average_grade_of_student();
        }
        return result/students.length;
    }

    @Override
    public int get_numberA_graders() {
        int counter = 0;
        for (int i = 0; i < 12; i++) {
            int intGrade = (int) students[i].get_average_grade_of_student();
            if (intGrade == 5)
                counter++;
        }

        return counter;
    }

    @Override
    public int get_numberF_graders() {
        int counter = 0;
        for (int i = 0; i < 12; i++) {
            for (byte j = 0; j < 3; j++) {
                if(students[i].getMark(j) == 2){
                    counter++;
                    break;
                }

            }

        }


        return counter;
    }


}
