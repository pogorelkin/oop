package main.Stud;

public class StudentProgress extends Student {
    private int[] marks = new int[3];
    /** for example we take four exams:
     * 0:math analysis
     * 1:english
     * 2:economics
      */
    public int getMark(byte subject) {
        return marks[subject];
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }

    public float get_average_grade_of_student(){
        float temp = 0;
        for (int i = 0; i < 3; i++) {
            temp +=  marks[i];

        };

        return temp/marks.length;
    }

    public void setMark(byte subject , int mark) {
        this.marks[subject] = mark;
    }




}

