package main.Stud;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;

public class StudentsDataTest extends MarksCalculationService {
    public void StartDemo() {
        Group group = new Group();
        String name = "Student";
        String family = "Familia";

        setGroupName("FirstGroup");
        System.out.println("Group name = " + getGroupName());

        System.out.println("Here the list of Students with their exams grades:");
        for (int i = 0; i < 12; i++) {//fill array with name and random grades [2,5]
            students[i] = new StudentProgress();
            students[i].setFirstName(name + i);
            students[i].setLastName(family + i + i);
            for (byte j = 0; j < 3; j++) {
                students[i].setMark(j, ThreadLocalRandom.current().nextInt(2, 5 + 1));
        int[] fives = {5, 5, 5};
        students[0].setMarks(fives);
            }
            System.out.println(i+1 + ": " + students[i].getFirstName() + " " + students[i].getLastName() + Arrays.toString(students[i].getMarks()) + students[i].get_average_grade_of_student());
        }

        System.out.println("Group average grade is " + get_groupAverage_grade() );
        System.out.println("Count of A grade students is " + get_numberA_graders()); //must be all 5
        System.out.println("Count if F grade students is " + get_numberF_graders()); // at least one F grade at any exam


    }

}
