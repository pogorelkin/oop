package main.Stud;

public interface MarkCalculatonServiceInterface {
    float get_groupAverage_grade();
    int get_numberA_graders();
    int get_numberF_graders();
}
