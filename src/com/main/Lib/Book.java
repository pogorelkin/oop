package main;

public class Book {
    private String author = null;
    private int counterPages = 0;
    private String name = null;


    private boolean isOccupied = false;

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean occupied) {
        isOccupied = occupied;
    }



    public Book() {
    }

    public Book(String author, String name, int counterPages) {
        this.author = author;
        this.counterPages = counterPages;
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCounterPages() {
        return counterPages;
    }

    public void setCounterPages(int counterPages) {
        this.counterPages = counterPages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void eraseData(){
        this.author = null;
        this.counterPages = 0;
        this.name = null;
        setOccupied(false);
    }
}
